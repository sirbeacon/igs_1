#include "stdafx.h"
#include <windows.h>     
#include <math.h>
#include <tchar.h>

#define M_PI 3.1415926535897932384626433832795

//��������� �������
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Line_Paint(HWND);
inline int ym(double);
inline int xn(double);

TCHAR cname[] = _T("Class");     //��� ������ ����
TCHAR title[] = _T("Scalable Fonts");  //��������� ����


									   //������� �������
int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdParam,
	int       nCmdShow)
{

	//����������� ������ ���� ����������
	//------------------------------------------------------------------------
	WNDCLASS wc;  // ��������� ��� ����������� ������ ���� ����������

	wc.style = 0;
	wc.lpfnWndProc = (WNDPROC)WndProc;         //������� ����
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;       //���������� ����������
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = cname;   //��� ������ ����   

	if (!RegisterClass(&wc))  //����������� ������ ���� ����������
		return 0;


	//�������� ���� ����������
	//------------------------------------------------------------------------
	HWND hWnd;       // ���������� ���� ����������

	hWnd = CreateWindow(cname,              //��� ������ ����
		title,               //��������� ����
		WS_OVERLAPPEDWINDOW, // ����� ����
		CW_USEDEFAULT,       // x
		CW_USEDEFAULT,       // y
		CW_USEDEFAULT,       // Width
		CW_USEDEFAULT,       // Height
		NULL,          //���������� ����-��������     
		NULL,          //���������� ����
		hInstance,     //���������� ����������
		NULL);

	if (!hWnd)
		return 0;


	// ������ ����. 
	//--------------------------------------------------------------------------
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);


	// ��������� ���� ��������� ���������
	MSG msg;    // ��������� ��� ������ � �����������

				//-------------------------------------------------------------------------
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg); // �������� ��������� ������� WndProc()
	}

	return 0;

}


//������� ����, ����������� ���������
LRESULT CALLBACK  WndProc(HWND hWnd, UINT message,
	WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{
		//��������� ��� ���������� ����
	case WM_PAINT:
		Line_Paint(hWnd);    //������� ��������� 
		break;

		//��������� ��� �������� ����
	case WM_DESTROY:
		PostQuitMessage(0);  //����� �� ����� ���������
		break;

		//��������� ��������� �� ���������
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;

}


//������� ���� ������ � ������� ������� ���������
double xLeft, xRight, yBottom, yTop;
//������� ���� ������ � �������� � ���������� �������
//���� ����������
int    nLeft, nRight, mBottom, mTop;



//������� ���������� �� ��������� WM_PAINT
void Line_Paint(HWND hwnd)
{
	//������� ���� � ������� ����������� � � ��������
	xLeft = -1; xRight = 2; yBottom = -2; yTop = 2;
	nLeft = 100; nRight = 450; mBottom = 350; mTop = 0;

	//������� ������� ����� ��� ��������� x � ������� y = sin(x)^2, y = exp(-x)/2
	const int N = 50;
	double corY_exp[N], corX[N], corY[N];
	double x, y, y_lg, dx = (xRight - xLeft) / (N - 1);

	for (int i = 0; i<N; i++)
	{
		x = xLeft + dx*i; y = sqrt(fabs(x - 1));
		y_lg = pow(x, 2)/2;
		corX[i] = x; corY[i] = y; corY_exp[i] = y_lg;
	}

	HDC hdc;                  //���������� ��������� ����������
	PAINTSTRUCT ps;           //��������� ��� ������ ��������    

							  //�������� �������� ���������� <hdc> ��� ���� <hwnd> 
	hdc = BeginPaint(hwnd, &ps);

	HBRUSH hbrush, hbrushOld;            //����������� ������
	HPEN hpen1, hpen2, hpen3, hpen4, hpenOld;    //����������� ������

										  //������� ����� <hbrush1>, ����� - ��������, ���� - �����
	hbrush = CreateSolidBrush(RGB(0, 0, 200));
	//�������� ����� <hbrush> � �������� ���������� <hdc>, 
	//���������� ���������� ������ ����� <hbrushOld>
	hbrushOld = (HBRUSH)SelectObject(hdc, hbrush);

	//������� ���� <hpen1>, ����� - ��������, ������� 3 �������, ���� - ����-������
	hpen1 = CreatePen(PS_SOLID, 3, RGB(255, 255, 0));
	//�������� ���� <hpen1> � �������� ���������� <hdc>, 
	//���������� ���������� ������� ���� <hpenOld>
	hpenOld = (HPEN)SelectObject(hdc, hpen1);

	//������ ������������� � ��������
	Rectangle(hdc, nLeft, mBottom, nRight, mTop);


	//������� ���� <hpen2>, ����� - ��������, ������� 1 ������, ���� - ����-�������
	hpen2 = CreatePen(PS_SOLID, 1, RGB(0, 255, 255));
	//�������� ���� <hpen2> � �������� ���������� <hdc>
	SelectObject(hdc, hpen2);

	int nb, ne, mb, me;
	POINT  pt;

	//������ ��� OX
	nb = xn(xLeft); mb = ym(0);
	MoveToEx(hdc, nb, mb, &pt);
	ne = xn(xRight); me = ym(0);
	LineTo(hdc, ne, me);

	//������ ��� OY
	nb = xn(0); mb = ym(yBottom);
	MoveToEx(hdc, nb, mb, &pt);
	ne = xn(0); me = ym(yTop);
	LineTo(hdc, ne, me);


	//������ ������ �� ���� ��������

	//������� ���� <hpen3>, ����� - ��������, ������� 2 ������, ���� - ����-�������
	hpen3 = CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
	hpen4 = CreatePen(PS_SOLID, 2, RGB(0, 255, 0));
	//�������� ���� <hpen3> � �������� ���������� <hdc>
	SelectObject(hdc, hpen3);

	//sin
	nb = xn(corX[0]);  mb = ym(corY[0]);
	MoveToEx(hdc, nb, mb, &pt);
	for (int i = 1; i < N; i++)
	{
		nb = xn(corX[i]);  mb = ym(corY[i]);
		LineTo(hdc, nb, mb);
	}

	SelectObject(hdc, hpen4);
	//exp
	nb = xn(corX[0]);  mb = ym(corY_exp[0]);
	MoveToEx(hdc, nb, mb, &pt);
	for (int i = 1; i < N; i++)
	{
		nb = xn(corX[i]);  mb = ym(corY_exp[i]);
		LineTo(hdc, nb, mb);
	}

	SelectObject(hdc, hbrushOld); //�������� ������ ����� <hbrushOld> � �������� ���������� <hdc>
	DeleteObject(hbrush);        //���������� ����� <hbrush>

	SelectObject(hdc, hpenOld);   //�������� ������ ���� <hpenOld> � �������� ���������� <hdc>
	DeleteObject(hpen1);         //���������� ���� <hpen1>
	DeleteObject(hpen2);         //���������� ���� <hpen2>
	DeleteObject(hpen3);         //���������� ���� <hpen3>


								 //��������� ��� ������ �� ��������
								 //���� ��������� ����������
	static LOGFONT lf;
	//����������� �������
	HFONT hFont, hFontOld;

	//������ ������ � ���������� ��������
	lf.lfHeight = 20;
	//������� ��������
	lf.lfWeight = FW_BOLD;
	//��� ������
	wcscpy_s(lf.lfFaceName, L"Courier New");
	//���� ������� � ������� ����� �������
	lf.lfEscapement = 0;

	//������� �����, �������� ���������� lf
	hFont = CreateFontIndirect(&lf);
	//�������� ����� <hFont> � �������� <hdc>
	//���������� ���������� ������� ������ <hFonthOld>
	hFontOld = (HFONT)SelectObject(hdc, hFont);

	//		SetBkColor(hdc,RGB(192,192,192));
	//		SetTextColor(hdc,RGB(255,255,255));

	SetBkColor(hdc, RGB(0, 0, 200));
	SetTextColor(hdc, RGB(0, 255, 0));

	//������ ������� ��������� �������
	TextOut(hdc, xn(-0.5), ym(-0.1), _T("(x^2)/2"), 10);

	SetTextColor(hdc, RGB(255, 0, 0));
	TextOut(hdc, xn(1), ym(-0.1), _T("sqrt(|x-1|)"), 11);

	//�������� ������ ����� <hFontOld> � �������� ���������� <hdc>
	SelectObject(hdc, hFontOld);
	//���������� ����� <hFont>
	DeleteObject(hFont);


	//����������� �������� ���������� <hdc> � ���� <hwnd>
	EndPaint(hwnd, &ps);

}

//������� �� x � ������� n
inline int xn(double x)
{
	return (int)((x - xLeft) / (xRight - xLeft)*(nRight - nLeft)) + nLeft;
}

//������� �� y � ������� m
inline int ym(double y)
{
	return (int)((y - yBottom) / (yTop - yBottom)*(mTop - mBottom)) + mBottom;
}
