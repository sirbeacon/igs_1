
#include <windows.h>     
#include <cmath>
#include <tchar.h>
#include "stdafx.h"
#include "Resource.h"
#include <iostream>

//��������� �������
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Polygon_OnDC(HWND);
void Ellipse_OnDC(HWND);

struct POINT2 {
	double x, y;
};

inline int ym(double);
inline int xn(double);

TCHAR cname[] = _T("Class");     //��� ������ ����
TCHAR title[] = _T("Menu");  //��������� ����


							 //������� �������
int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdParam,
	int       nCmdShow)
{

	//����������� ������ ���� ����������
	//------------------------------------------------------------------------
	WNDCLASS wc;  // ��������� ��� ����������� ������ ���� ����������

	wc.style = 0;
	wc.lpfnWndProc = (WNDPROC)WndProc;         //������� ����
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;       //���������� ����������
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(LTGRAY_BRUSH);
	wc.lpszMenuName = (LPCTSTR)IDR_MENU1;
	wc.lpszClassName = cname;   //��� ������ ����   

	if (!RegisterClass(&wc))  //����������� ������ ���� ����������
		return 0;


	//�������� ���� ����������
	//------------------------------------------------------------------------
	HWND hWnd;       // ���������� ���� ����������

	hWnd = CreateWindow(cname,              //��� ������ ����
		title,               //��������� ����
		WS_OVERLAPPEDWINDOW, // ����� ����
		CW_USEDEFAULT,       // x
		CW_USEDEFAULT,       // y
		CW_USEDEFAULT,       // Width
		CW_USEDEFAULT,       // Height
		NULL,          //���������� ����-��������     
		NULL,          //���������� ����
		hInstance,     //���������� ����������
		NULL);

	if (!hWnd)
		return 0;


	// ������ ����. 
	//--------------------------------------------------------------------------
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);


	// ��������� ���� ��������� ���������
	MSG msg;    // ��������� ��� ������ � �����������

				//-------------------------------------------------------------------------
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg); // �������� ��������� ������� WndProc()
	}

	return 0;

}


//������� ����, ����������� ���������
LRESULT CALLBACK  WndProc(HWND hWnd, UINT message,
	WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{
		//��������� ��� ���������� ����
		//		case WM_PAINT:
		//			Line_Paint(hWnd);    //������� ��������� 
		//			break;


		//��������� �� ������ ����
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_FILE_EXIT:
			PostQuitMessage(0);
			break;

		case ID_FILE_CLEAR:
			InvalidateRect(hWnd, NULL, TRUE);
			break;

		case ID_GRAPHICS_ELLIPSE:
			Ellipse_OnDC(hWnd);
			break;

		case ID_GRAPHICS_POLYGON:
			Polygon_OnDC(hWnd);
			break;
		}
		break;


		//��������� ��� �������� ����
	case WM_DESTROY:
		PostQuitMessage(0);  //����� �� ����� ���������
		break;

		//��������� ��������� �� ���������
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;

}


//������� ���� ������ � ������� ������� ���������
double xLeft, xRight, yBottom, yTop;
//������� ���� ������ � �������� � ���������� �������
//���� ����������
int    nLeft, nRight, mBottom, mTop;


void Polygon_OnDC(HWND hwnd)
{
	RECT rc;
	GetClientRect(hwnd, &rc); //���� ������� ���������� ������� ���� <hwnd>

							  //������� ���� � ������� ����������� � � ��������
	xLeft = -10; xRight = 10; yBottom = -6; yTop = 6;
	nLeft = rc.left; nRight = rc.right; mBottom = rc.bottom; mTop = rc.top;

	double masht; //����������� ������� ���������� ��� ��������
				  //�� ������� � �������� �����������
	masht = (xRight - xLeft)*(mBottom - mTop) / (yTop - yBottom) / (nRight - nLeft);

	HDC hdc;
	hdc = GetDC(hwnd);

	HBRUSH hbrush, hbrushOld;
	HPEN hpen1, hpen2, hpenOld;
	hpen1 = CreatePen(PS_SOLID, 1, RGB(0, 255, 255));
	hpenOld = (HPEN)SelectObject(hdc, hpen1);

	int nb, ne, mb, me;
	//������ ��� OX
	nb = xn(xLeft); mb = ym(0);
	MoveToEx(hdc, nb, mb, NULL);
	ne = xn(xRight); me = ym(0);
	LineTo(hdc, ne, me);

	//������ ��� OY
	nb = xn(0); mb = ym(yBottom);
	MoveToEx(hdc, nb, mb, NULL);
	ne = xn(0); me = ym(yTop);
	LineTo(hdc, ne, me);

	const int vertex = 10;
	POINT2 Vt[vertex];
	Vt[0].x = 1.175; Vt[0].y = 1.618;
	Vt[1].x = 0; Vt[1].y = 6.0;
	Vt[2].x = -1.17; Vt[2].y = 1.618;
	Vt[3].x = -5.706; Vt[3].y = 1.854;
	Vt[4].x = -1.902;  Vt[4].y = -0.618;
	Vt[5].x = -3.526; Vt[5].y = -4.854;
	Vt[6].x = 0;  Vt[6].y = -2.0;
	Vt[7].x = 3.526; Vt[7].y = -4.85;
	Vt[8].x = 1.902;  Vt[8].y = -0.618;
	Vt[9].x = 5.706; Vt[9].y = 1.854;


	POINT P[vertex];
	for (int i = 0; i<vertex; i++)
	{
		P[i].x = xn(Vt[i].x); P[i].y = ym(Vt[i].y);
	}

	hpen2 = CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
	SelectObject(hdc, hpen2);

	hbrush = CreateSolidBrush(RGB(0, 200, 255));
	hbrushOld = (HBRUSH)SelectObject(hdc, hbrush);

	Polygon(hdc, P, vertex);

	SelectObject(hdc, hbrushOld);
	DeleteObject(hbrush);
	SelectObject(hdc, hpenOld);
	DeleteObject(hpen1);
	DeleteObject(hpen2);

	LOGFONT lf;
	HFONT hFont, hFontOld;

	double dx, dy, alf;
	for (int j = 0; j<vertex-1; j++)
	{
		dx = Vt[j + 1].x - Vt[j].x;
		dy = Vt[j + 1].y - Vt[j].y;
		alf = atan2(dy*masht, dx);

		memset(&lf, 0, sizeof(LOGFONT));
		wcscpy_s(lf.lfFaceName, L"Courier New");
		lf.lfHeight = 18;
		lf.lfWeight = FW_BOLD;
		lf.lfEscapement = LONG(573 * alf);
		hFont = CreateFontIndirect(&lf);
		if (j == 0)
			hFontOld = (HFONT)SelectObject(hdc, hFont);
		else
			SelectObject(hdc, hFont);

		SetBkColor(hdc, RGB(192, 192, 192));
		SetTextColor(hdc, RGB(255, 255, 255));
		TextOut(hdc, xn(Vt[j].x - 1.25*sin(alf)),
			ym(Vt[j].y + 1.25*cos(alf)), _T(" Polygon "), 9);
	}

	SelectObject(hdc, hFontOld);
	DeleteObject(hFont);
	ReleaseDC(hwnd, hdc);
}

void Ellipse_OnDC(HWND hwnd)
{
	/*�������� ������������� ������� ���� ������
	HRGN hrgn1 = CreateRectRgn(nLeft, mTop, nRight, mBottom);

	//�������� ���������� ������� ����� ������
	HBRUSH hbrush1 = CreateSolidBrush(RGB(0xFF, 0xFF, 0xFF));
	FillRgn(hdc, hrgn1, hbrush1);

	//������������ ������� ������ �����������
	SelectClipRgn(hdc, hrgn1);*/
	RECT rc;
	GetClientRect(hwnd, &rc); //���� ������� ���������� ������� ���� <hwnd>

							  //������� ���� � ������� ����������� � � ��������
	xLeft = -10; xRight = 10; yBottom = -6; yTop = 6;
	nLeft = rc.left; nRight = rc.right; mBottom = rc.bottom; mTop = rc.top;

	double masht; //����������� ������� ���������� ��� ��������
				  //�� ������� � �������� �����������
	masht = (xRight - xLeft)*(mBottom - mTop) / (yTop - yBottom) / (nRight - nLeft);

	HDC hdc;
	hdc = GetDC(hwnd);

	HBRUSH hbrush, hbrushOld, hbrush2;
	HPEN hpen1, hpen2, hpenOld;
	hpen1 = CreatePen(PS_SOLID, 1, RGB(0, 255, 255));
	hpenOld = (HPEN)SelectObject(hdc, hpen1);

	int nb, ne, mb, me;
	//������ ��� OX
	nb = xn(xLeft); mb = ym(0);
	MoveToEx(hdc, nb, mb, NULL);
	ne = xn(xRight); me = ym(0);
	LineTo(hdc, ne, me);

	//������ ��� OY
	nb = xn(0); mb = ym(yBottom);
	MoveToEx(hdc, nb, mb, NULL);
	ne = xn(0); me = ym(yTop);
	LineTo(hdc, ne, me);

	//�������
	LOGFONT lf;
	HFONT hFont1, hFontOld;

	memset(&lf, 0, sizeof(LOGFONT));
	wcscpy_s(lf.lfFaceName, L"Arial");

	lf.lfHeight = 45;
	lf.lfWeight = FW_BOLD;
	lf.lfEscapement = 900;

	hFont1 = CreateFontIndirect(&lf);
	hFontOld = (HFONT)SelectObject(hdc, hFont1);

	TextOut(hdc, xn(-9), ym(-2), _T(" E l l i p s e "), 15);


	//������� �������
	double x1 = -8, x2 = 8, y1 = 5, y2 = -5, cx1 = 7.5, cy1 = 0.5, cx2 = -7.5;

	hpen2 = CreatePen(PS_SOLID, 2, RGB(255, 0, 0));
	SelectObject(hdc, hpen2);

	hbrush2 = CreateSolidBrush(RGB(0, 200, 255));
	hbrushOld = (HBRUSH)SelectObject(hdc, hbrush2);

	Chord(hdc, xn(x1), ym(y1), xn(x2), ym(y2), xn(cx1), ym(cy1), xn(cx2), ym(cy1));


	//��������������� �������� �� ��������� � ���������� ��������� GDI-�������
	SelectObject(hdc, hbrushOld);
	//DeleteObject(hbrush1);
	DeleteObject(hbrush2);

	SelectObject(hdc, hpenOld);
	DeleteObject(hpen1);
	DeleteObject(hpen2);

	//SelectObject(hdc, hFontOld);
	//DeleteObject(hFont1);
	//DeleteObject(hrgn1);
	ReleaseDC(hwnd, hdc);
}


	//������� �� x � ������� n
	inline int xn(double x)
	{
		return (int)((x - xLeft) / (xRight - xLeft)*(nRight - nLeft)) + nLeft;
	}

	//������� �� y � ������� m
	inline int ym(double y)
	{
		return (int)((y - yBottom) / (yTop - yBottom)*(mTop - mBottom)) + mBottom;
	}
